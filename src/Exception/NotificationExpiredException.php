<?php

/**
 * @file
 * Contains Mpx\Exception\NotificationExpiredException.
 */

namespace Mpx\Exception;

class NotificationExpiredException extends Exception { }
