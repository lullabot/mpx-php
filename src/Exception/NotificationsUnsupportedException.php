<?php

/**
 * @file
 * Contains Mpx\Exception\NotificationsUnsupporteException.
 */

namespace Mpx\Exception;

class NotificationsUnsupportedException extends Exception { }
