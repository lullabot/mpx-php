<?php

/**
 * @file
 * Contains Mpx\Exception\ObjectNotFoundException.
 */

namespace Mpx\Exception;

class ObjectNotFoundException extends Exception { }
